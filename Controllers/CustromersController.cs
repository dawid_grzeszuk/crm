﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CRMapp.Infrastructure;
using CRMapp.Model;
using Microsoft.AspNetCore.Mvc;

namespace CRMapp.Controllers
{
    [Route("api/[controller]")]
    public class CustromersController : Controller
    {

        private readonly MongoRepository _customerRepository;
        
        public CustromersController()
        {
            _customerRepository = new MongoRepository();
        }
        
        // GET api/values
        [HttpGet]
        public async Task<IActionResult> Get(string filter)
        {
            return Ok(await _customerRepository.GetAll(filter));
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public async Task<IActionResult> Post(Customer arg)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await _customerRepository.Insert(arg);
            return Ok();
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            var result = await _customerRepository.Delete(id);
            return result ? (IActionResult) Ok() : BadRequest();
        }
    }
}