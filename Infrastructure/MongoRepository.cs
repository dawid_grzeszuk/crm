﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CRMapp.Model;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace CRMapp.Infrastructure
{
    public class MongoRepository
    {
        private readonly IMongoCollection<Customer> _collection;
        
        public MongoRepository()
        {
            var client = new MongoClient("mongodb://localhost:27017");
            var database = client.GetDatabase("CRM");
            _collection = database.GetCollection<Customer>("Customers");
        }

        public async Task<List<Customer>> GetAll(string filter)
        {
          var query = _collection.AsQueryable().Where(x=>x.FirstName.Contains(filter) 
                                            || x.LastName.Contains(filter));

          return await query.ToListAsync();
        }

        public async Task Insert(Customer customer)
        {
            await _collection.InsertOneAsync(customer);
        }
        
        public async Task<bool> Delete(string id)
        {            
            ObjectId.TryParse(id,out var xd);
            {
                return false;
            }

            var result = await _collection.FindAsync(x=>x.Id == xd);

            if (result == null)
            {
                return false;
            }
            
            await _collection.DeleteOneAsync(x=>x.Id == xd);
            return true;
        }
    }
}